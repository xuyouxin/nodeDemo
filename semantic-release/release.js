
const execa = require('execa');

const fs = require("fs")

try {

    const branch = 'stage1';

    execa.shellSync('semantic-release -t \'Release-${version}\' ' + ` -b ${branch}`);

    const packageText = fs.readFileSync('./package.json', {encoding: 'utf-8'});
    const packageJson = JSON.parse(packageText);
    const version = packageJson['version'];

    console.log('version>>', version);

    execa.shellSync(`npx lerna version ${version} --no-git-tag-version --exact --yes`);


    execa.shellSync(`cd ../ && npm version ${version} --no-git-tag-version --allow-same-version`);

} catch (e) {
    console.error(e)
}

