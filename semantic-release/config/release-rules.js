module.exports = [
  {message: /Merge\s+branch\s+'release\/.*'\s+into\s+'master'/, release: 'minor'},
  {message: /.*/, release: 'patch'}
];
